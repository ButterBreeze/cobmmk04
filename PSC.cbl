
       01  TITLE-LINE-FURN.
           05  FILLER          PIC X(6)    VALUE"DATE: ".
           05  O-MONTH         PIC 99.
           05  FILLER          PIC X       VALUE "/".
           05  O-DAY           PIC 99.
           05  FILLER          PIC X       VALUE "/".
           05  O-YEAR          PIC  9999.
           05  FILLER          PIC X(35)   VALUE SPACES.
           05  FILLER          PIC X(28)   VALUE "JUST FITS FURNITURE EM
      -                        "PORIUM".
           05  FILLER          PIC X(44)   VALUE SPACES.
           05  FILLER          PIC X(6)    VALUE "PAGE: ".
           05  O-PGCTR-FURN    PIC Z9.

       01  CONTENTS-LINE-FURN.
           05  FILLER          PIC X(56)   VALUE SPACES.
           05  FILLER          PIC X(19)   VALUE "WEEKLY SALES REPORT".

       01  HDG-1-FURN.
           05  FILLER          PIC X(33)   VALUE SPACES.
           05  FILLER          PIC X(9)    VALUE "FURNITURE".
           05  FILLER          PIC X(49)   VALUE SPACES.
           05  FILLER          PIC X(9)    VALUE "FURNITURE".

       01  HDG-2-FURN.
           05  FILLER          PIC X(33)   VALUE SPACES.
           05  FILLER          PIC X(4)    VALUE "TYPE".
           05  FILLER          PIC X(53)   VALUE SPACES.
           05  FILLER          PIC X(10)   VALUE "TYPE TOTAL".

       01  DFURN-1.
           05  FILLER          PIC X(33)   VALUE SPACES.
           05  O-FURN-NAME     PIC X(22).
           05  FILLER          PIC X(32)   VALUE SPACES.
           05  O-FURN-TOTAL    PIC ZZ,ZZZZ,ZZ.99.

       01  GFURN-1.
           05  FILLER          PIC X(33)   VALUE SPACES.
           05  FILLER         PIC X(21)   VALUE "FURNITURE GRAND TOTAL".
           05  FILLER          PIC X(29)   VALUE SPACES.
           05  O-FURN-GRAND    PIC $$,$$$,$$$,$$$.99.

      *************************************************************************


       01  WK-TITLE.
           05  FILLER          PIC X(6)    VALUE"DATE: ".
           05  O-WK-MONTH         PIC 99.
           05  FILLER          PIC X       VALUE "/".
           05  O-WK-DAY           PIC 99.
           05  FILLER          PIC X       VALUE "/".
           05  O-WK-YEAR          PIC  9999.
           05  FILLER          PIC X(36)   VALUE SPACES.
           05  FILLER          PIC X(28)   VALUE "JUST FITS FURNITURE EM
      -                        "PORIUM".
           05  FILLER          PIC X(44)   VALUE SPACES.
           05  FILLER          PIC X(6)    VALUE "PAGE: ".
           05  O-PGCTR-WK      PIC Z9.

       01  WK-REPORT-LINE.
           05  FILLER          PIC X(56)   VALUE SPACES.
           05  FILLER          PIC X(19)   VALUE "WEEKLY SALES REPORT".

       01  WKHDG-1.
           05  FILLER          PIC X(11)   VALUE "SALESPERSON".
           05  FILLER          PIC X(21)   VALUE SPACES.
           05  FILLER          PIC X(6)    VALUE "SUNDAY".
           05  FILLER          PIC X(7)    VALUE SPACES.
           05  FILLER          PIC X(6)    VALUE "MONDAY".
           05  FILLER          PIC X(6)    VALUE SPACES.
           05  FILLER          PIC X(7)    VALUE "TUESDAY".
           05  FILLER          PIC X(4)    VALUE SPACES.
           05  FILLER          PIC X(9)    VALUE "WEDNESDAY".
           05  FILLER          PIC X(5)    VALUE SPACES.
           05  FILLER          PIC X(8)    VALUE "THURSDAY".
           05  FILLER          PIC X(7)    VALUE SPACES.
           05  FILLER          PIC X(6)    VALUE "FRIDAY".
           05  FILLER          PIC X(5)    VALUE SPACES.
           05  FILLER          PIC X(8)    VALUE "SATURDAY".
           05  FILLER          PIC X(10)   VALUE SPACES.
           05  FILLER          PIC X(6)    VALUE "WEEKLY".

       01  WKHDG-2.
           05  FILLER          PIC XXX     VALUE SPACES.
           05  FILLER          PIC X(4)    VALUE "NAME".
           05  FILLER          PIC X(119)  VALUE SPACES.
           05  FILLER          PIC X(6)    VALUE "TOTALS".

       01  WKD-1.
           05  O-SALES-NAME    PIC X(25).
           05  FILLER          PIC X(3)    VALUE SPACES.
           05  O-SUNDAY        PIC ZZZ,ZZZ.99.
           05  FILLER          PIC X(3)    VALUE SPACES.
           05  O-MONDAY        PIC ZZZ,ZZZ.99.
           05  FILLER          PIC X(3)    VALUE SPACES.
           05  O-TUESDAY       PIC ZZZ,ZZZ.99.
           05  FILLER          PIC X(3)    VALUE SPACES.
           05  O-WEDNESDAY     PIC ZZZ,ZZZ.99.
           05  FILLER          PIC X(3)    VALUE SPACES.
           05  O-THURSDAY      PIC ZZZ,ZZZ.99.
           05  FILLER          PIC X(3)    VALUE SPACES.
           05  O-FRIDAY        PIC ZZZ,ZZZ.99.
           05  FILLER          PIC X(3)    VALUE SPACES.
           05  O-SATURDAY      PIC ZZZ,ZZZ.99.
           05  FILLER          PIC X(3)    VALUE SPACES.
           05  O-WEEKLYTOTALS  PIC ZZ,ZZZ,ZZZ.99.

       01  WKG-1.
           05  FILLER          PIC X(25)   VALUE "GRAN TOTALS: ".
           05  O-SUN-GRAND     PIC $$$,$$$,$$$.99.
           05  FILLER          PIC X(12)   VALUE SPACES.
           05  O-TUE-GRAND     PIC $$$,$$$,$$$.99.
           05  FILLER          PIC X(12)   VALUE SPACES.
           05  O-THU-GRAND     PIC $$$,$$$,$$$.99.
           05  FILLER          PIC X(13)   VALUE SPACES.
           05  O-SAT-GRAND     PIC $$$,$$$,$$$.99.

       01  WKG-2.
           05  FILLER          PIC X(37)   VALUE SPACES.
           05  O-MON-GRAND     PIC $$$,$$$,$$$.99.
           05  FILLER          PIC X(12)   VALUE SPACES.
           05  O-WED-GRAND     PIC $$$,$$$,$$$.99.
           05  FILLER          PIC X(12)   VALUE SPACES.
           05  O-FRI-GRAND     PIC $$$,$$$,$$$.99.
           05  FILLER          PIC X(12)   VALUE SPACES.
           05  O-GRAND-GRAND   PIC $$,$$$,$$$,$$$.99.
