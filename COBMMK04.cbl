       IDENTIFICATION DIVISION.
       PROGRAM-ID.     COBMMK04.
       DATE-WRITTEN.   4/24/2018.
      ******************************************************************
      *       THIS PROGRAM VALIDATES RESERVATIONS AND CREATES A FILE   *
      *       OF RECORDS OF  *
      *       PUT INTO A REPORT FILE                                   *
      ******************************************************************

       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.

       FILE-CONTROL.


       COPY FILEDEFS.


       WORKING-STORAGE SECTION.
       01  MATHS-SHTUFFS.
           05  X       PIC 999.
           05  Y       PIC 999.
           05  PGCTR   PIC 99      VALUE 0.
           05  MORE-RECS           PIC X(25)       VALUE "YES".
           05  FURN-GRAND  PIC 9(12)       VALUE 0.

       01  CURRENT-DATE-AND-TIME.
           05  SYS-DATE.
               10  SYS-YEAR      PIC 9(4).
               10  SYS-MONTH     PIC 99.
               10  SYS-DAY       PIC 99.
           05  SYS-TIME          PIC X(11).

       COPY TABLES.
       COPY PSC.


       PROCEDURE DIVISION.
       0000-MAIN.
           PERFORM 1000-INIT.
           PERFORM 2000-MAINLINE
               UNTIL MORE-RECS NOT = "YES".
           PERFORM 3000-OUTPUT.
           CLOSE INPUT-FILE.
           CLOSE OUTPUT-FILE.
           STOP RUN.
       1000-INIT.
           MOVE FUNCTION CURRENT-DATE TO CURRENT-DATE-AND-TIME.
           MOVE SYS-MONTH TO O-MONTH.
           MOVE SYS-YEAR TO O-YEAR.
           MOVE SYS-DAY TO O-DAY.
           MOVE SYS-MONTH TO O-WK-MONTH.
           MOVE SYS-YEAR TO O-WK-YEAR.
           MOVE SYS-DAY TO O-WK-DAY.

           OPEN INPUT INPUT-FILE.
           OPEN OUTPUT OUTPUT-FILE.

           MOVE 1 TO X.
           MOVE 1 TO Y.

           PERFORM VARYING X FROM 1 BY 1 UNTIL X =16
               PERFORM VARYING Y FROM 1 BY 1 UNTIL Y = 9
                   MOVE 1 TO SP-DAY(X,Y)
                   END-PERFORM
               MOVE SPACE TO SP-NAME(X)
           END-PERFORM.

           PERFORM VARYING X FROM 1 BY 1 UNTIL X = 10
               MOVE 0 TO FURN-NUM-TOTAL(X)
           END-PERFORM.

           PERFORM VARYING X FROM 1 BY 1 UNTIL X = 9
               MOVE 0 TO DAY-GRAND(X)
           END-PERFORM.

           PERFORM 9100-HDG.
           PERFORM 9200-READ.

       2000-MAINLINE.
           PERFORM 2200-CALC.
           PERFORM 9200-READ.


       2200-CALC.
           MOVE I-ST-SLM-NAME TO SP-NAME(I-ST-SLM-NUM).
           ADD I-ST-AMOUNT TO SP-DAY(I-ST-SLM-NUM, I-ST-DAY).
           ADD I-ST-AMOUNT TO FURN-NUM-TOTAL(I-ST-FUR-CODE).
           ADD I-ST-AMOUNT TO SP-DAY(I-ST-SLM-NUM, 8).
           ADD I-ST-AMOUNT TO FURN-GRAND.
           ADD I-ST-AMOUNT TO DAY-GRAND(I-ST-DAY).
           ADD I-ST-AMOUNT TO DAY-GRAND(8).

       3000-OUTPUT.
           PERFORM VARYING X FROM 1 BY 1 UNTIL X = 10
               MOVE FURN-NAME-NUM(X) TO O-FURN-NAME
               MOVE FURN-NUM-TOTAL(X) TO O-FURN-TOTAL
               WRITE PRTLINE FROM DFURN-1 AFTER ADVANCING 1 LINE
                   AT EOP PERFORM 9100-HDG
           END-PERFORM.

           MOVE FURN-GRAND TO O-FURN-GRAND.
           WRITE PRTLINE FROM GFURN-1 AFTER ADVANCING 2 LINES
               AT EOP PERFORM 9100-HDG.

           ADD 1 TO PGCTR.
           WRITE PRTLINE FROM WK-TITLE
               AFTER ADVANCING PAGE.
           WRITE PRTLINE FROM WK-REPORT-LINE AFTER ADVANCING 1 LINE

           WRITE PRTLINE FROM WKHDG-1 AFTER ADVANCING 2 LINES.
           WRITE PRTLINE FROM WKHDG-2 AFTER ADVANCING 1 LINE.

           PERFORM VARYING X FROM 1 BY 1 UNTIL X = 16
               IF SP-NAME(X) NOT EQUAL TO SPACE
                   MOVE SP-NAME(X) TO O-SALES-NAME
                   MOVE SP-DAY(X, 1) TO O-SUNDAY
                   MOVE SP-DAY(X, 2) TO O-MONDAY
                   MOVE SP-DAY(X, 3) TO O-TUESDAY
                   MOVE SP-DAY(X, 4) TO O-WEDNESDAY
                   MOVE SP-DAY(X, 5) TO O-THURSDAY
                   MOVE SP-DAY(X, 6) TO O-FRIDAY
                   MOVE SP-DAY(X, 7) TO O-SATURDAY
                   MOVE SP-DAY(X, 8) TO O-WEEKLYTOTALS
                   WRITE PRTLINE FROM WKD-1
                       AFTER ADVANCING 1 LINE AT EOP
                           PERFORM 9100-HDG
               END-IF
           END-PERFORM.
           MOVE DAY-GRAND(1) TO O-SUN-GRAND.
           MOVE DAY-GRAND(2) TO O-MON-GRAND.
           MOVE DAY-GRAND(3) TO O-TUE-GRAND.
           MOVE DAY-GRAND(4) TO O-WED-GRAND.
           MOVE DAY-GRAND(5) TO O-THU-GRAND.
           MOVE DAY-GRAND(6) TO O-FRI-GRAND.
           MOVE DAY-GRAND(7) TO O-SAT-GRAND.
           MOVE DAY-GRAND(8) TO O-GRAND-GRAND.

           WRITE PRTLINE FROM WKG-1 AFTER ADVANCING 2 LINES.
           WRITE PRTLINE FROM WKG-2 AFTER ADVANCING 1 LINE.

       9100-HDG.
           ADD 1 TO PGCTR.
           MOVE PGCTR TO O-PGCTR-FURN.
           WRITE PRTLINE FROM TITLE-LINE-FURN
               AFTER ADVANCING PAGE.
           WRITE PRTLINE FROM CONTENTS-LINE-FURN
               AFTER ADVANCING 1 LINE.
           WRITE PRTLINE FROM HDG-1-FURN
               AFTER ADVANCING 2 LINES.
           WRITE PRTLINE FROM HDG-2-FURN
               AFTER ADVANCING 1 LINE.

       9200-READ.
           READ INPUT-FILE
               AT END MOVE "NO" TO MORE-RECS.

       END PROGRAM COBMMK04.
